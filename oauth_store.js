import { observable, action } from 'mobx'
import { parse } from 'qs'

export default class OAuthStore {
  constructor(config) {
    this.config = config
  }

  @observable name = null
  @observable email = null
  @observable token = null
  @observable type = null
  @observable uri = null
  @observable loaded = false

  @action.bound
  setLoaded(value) {
    this.loaded = value
  }

  @action.bound
  login(url) {
    const res = parse(url.replace(`${this.config.oauthProtocol}://login?`, ''))
    res.email = res.email.replace('#_=_', '')
    this.name = res.name
    this.email = res.email
    this.token = res.token
  }

  setType(type) {
    this.type = type
    this.uri = `${this.config.apiUrl}/oauth/${type}`
  }
}
