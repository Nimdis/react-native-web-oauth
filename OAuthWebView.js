import React, { Component } from 'react'
import { WebView, Platform } from 'react-native'
import { observer, inject } from 'mobx-react'

@inject(['oauthStore'])
@observer
export default class OAuthWebView extends Component {
  constructor(props) {
    super(props)
    this._onNavigationStateChange = this._onNavigationStateChange.bind(this)
    this._onLoadEnd = this._onLoadEnd.bind(this)
    this._onError = this._onError.bind(this)
    this.props.oauthStore.setLoaded(false)
  }

  _onNavigationStateChange(state) {
    const oauthProtocol = this.props.oauthStore.config.oauthProtocol

    const regex = new RegExp(`${oauthProtocol}:\/\/login`, 'g')

    if (this.props.oauthStore.loaded && state.url.match(regex)) {
      this.props.oauthStore.login(state.url)
      return this.props.success()
    }
  }

  _onError(e) {
    console.log(e)
  }

  _onLoadEnd() {
    this.props.oauthStore.setLoaded(true)
  }

  render() {
    const uri = this.props.oauthStore.uri
    return (
      <WebView
        source={{ uri }}
        userAgent="Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19"
        onNavigationStateChange={this._onNavigationStateChange}
        onLoadEnd={this._onLoadEnd}
        onError={this._onError}
      />
    )
  }
}
