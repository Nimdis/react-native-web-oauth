# Simple web OAuth library for react native

Currently supports only facebook and google

## Installation

- Add to your package.json

```
"react-native-web-oauth": "https://gitlab.com/Nimdis/react-native-web-oauth.git"
```

- Register google and facebook apps; configure linking. How to do it described [here](http://rationalappdev.com/logging-into-react-native-apps-with-facebook-or-google/)

## Usage

Add to your app stores OAuthStore

```
import OAuthStore from 'react-native-web-oauth/oauth_store'
import config from '../config'

export default {
  oauthStore: new OAuthStore(config)
}
```

Config __must__ contains apiUrl key with value similar to ```http://test-app.nimdis.com/api/v1```

Login Screen example:

```
import React, { Component } from 'react'
import {
  Text,
  View,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import { NavigationActions } from 'react-navigation'
import { observer, inject } from 'mobx-react'

@inject(['oauthStore'])
@observer
export default class OAuthScreen extends Component {
  constructor(props) {
    super(props)
    StatusBar.setHidden(true, 'none')
  }

  _goToOAuthWebView(type) {
    this.props.authStore.setType(type)
    return this.props.navigation.dispatch(NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'OAuthWebView' })]
    }))
  }

  render() {
    return (
      <View>
        <View>
          <TouchableOpacity onPress={() => this._goToOAuthWebView('facebook')}>
            <Text>Facebook</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this._goToOAuthWebView('google')}>
            <Text>Google</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
```

OAuthWebView screen example:

```
import React, { Component } from 'react'
import { NavigationActions } from 'react-navigation'
import OAuthWebViewScreen from 'react-native-web-oauth/OAuthWebView'

export default class AuthWebViewScreen extends Component {
  constructor(props) {
    super(props)
    this._success = this._success.bind(this)
  }

  _success(state) {
    console.log('here')
    return this.props.navigation.dispatch(NavigationActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName: 'SignedIn' })]
    }))
  }

  render() {
    return (
      <OAuthWebViewScreen success={this._success} />
    )
  }
}
```

## Backend integration

Backend must have a resource with name ```/oauth``` and actions that represents each auth method. Ex: ```http://test-app.nimdis.com/api/v1/oauth/google```
